import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})

export class ContactComponent implements OnInit {

  contactForm: FormGroup;
  formData: object;
  formClean = false;

  // transporter;
  // mailOptions;
  constructor(private builder: FormBuilder,
    private _adb: AngularFireDatabase) {
    this.contactForm = this.builder.group({
      name: [null, Validators.required],
      email: [null, Validators.required],
      subject: [null, Validators.required],
      message: [null, Validators.required],
    });
    // this.transporter = nodemailer.createTransport({
    //     service: 'gmail',
    //     auth: {
    //       user: 'fzam0r1te@gmail.com',
    //       pass: 'fzam0r1tefzam0r1te'
    //     }
    // });

    // this.mailOptions = {
    //   from: 'fzam0r1te@gmail.com',
    //   to: 'zamorite@gmail.com',
    //   subject: 'Testing nodemailer',
    //   html: '<p>Yippee!, it worked!</p>',
    // };

    this.contactForm.valueChanges.subscribe(
      () => {
        this.formClean = this.contactForm.valid;
      }
    );
  }

  getFormData() {
    this.formData = {
      name: this.contactForm.controls.name.value,
      email: this.contactForm.controls.email.value,
      subject: this.contactForm.controls.subject.value,
      message: this.contactForm.controls.message.value,
    };

    console.log(this.formData);
    this.contactForm.reset();

    this._adb.database.ref('enquiries')
      .push({
        emailData: this.formData
      });

    // this.transporter.sendMail(this.mailOptions)
    // .then((result) => {
    //   console.log('Result: ', result);
    // }).catch((err) => {
    //   console.log('Error message: ', err.message);
    // });
  }

  ngOnInit() {
  }

}
