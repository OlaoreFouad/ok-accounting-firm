import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private _title: Title,
  private _router: Router) {
    this._title.setTitle('Home | OK Accounting Firm');
   }

  ngOnInit() {
    const element = document.getElementById('video');
    console.log(element);
  }

}
