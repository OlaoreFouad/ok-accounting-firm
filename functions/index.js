const functions = require('firebase-functions');

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

'use strict';
const nodemailer = require('nodemailer');

exports.sendMailFn = functions.database.ref('/enquiries/{emailkey}').onWrite(
    change => {
        var data = change.after.val().emailData;

        this.transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: 'fzam0r1te@gmail.com',
                pass: 'fzam0r1tefzam0r1te'
            }
        });

        this.mailOptions = {
            from: 'fzam0r1te@gmail.com',
            to: 'zamorite@gmail.com',
            subject: `New Message from ${data.name}`,
            html: `<h3>${data.subject}</h3><p><b>Message:</b><br>${data.message}</p><p><b>${data.name}</b>,<br>${data.email}</p>`,
        };

        this.transporter.sendMail(this.mailOptions)
        .then((result) => {
            console.log('Result: ', result);
        }).catch((err) => {
            console.log('Error message: ', err.message);
        });
    }
);
